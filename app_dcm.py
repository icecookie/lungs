import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from werkzeug.contrib.fixers import ProxyFix
from flask import Flask, render_template, request, send_from_directory
import flask
import io
from configparser import ConfigParser
from PIL import Image
import keras.backend as K
import grad_cam as gc
from models.densenet121 import get_model
import tensorflow as tf
import numpy as np
import cv2
import matplotlib.pyplot as plt
from werkzeug.utils import secure_filename
from gevent.wsgi import WSGIServer
import dicom
import scipy.misc
from datetime import datetime


app = Flask(__name__, template_folder='templates')
app.wsgi_app = ProxyFix(app.wsgi_app)


def target_category_loss(x, category_index, nb_classes):
    return tf.multiply(x, K.one_hot([category_index], nb_classes))


global model
global class_names
config_file = "sample_config.ini"
cp = ConfigParser()
cp.read(config_file)
class_names = cp["DEFAULT"].get("class_names").split(",")
weights_path = 'best_weights.h5'
model = get_model(class_names)
model.load_weights(weights_path)

def prepare_image(image, target, preparation=True, reverse=False):

    image = image.convert("RGB")

    image = image.resize(target)
    image = np.asarray(image)[:, :, ::-1]

    if reverse:
        image = cv2.bitwise_not(image)

    if preparation:

        image = image / 255
        mean = np.array([0.485, 0.456, 0.406])
        std = np.array([0.229, 0.224, 0.225])

        return (image - mean) / std #image to predict

    else:
        return image #original image


def grad_cam(model, class_names, x_orig, x_model, pred):

    x_orig = 255 * x_orig.squeeze()
    x_model_i = x_model

    #print("** perform grad cam **")
    predicted_class = np.argmax(pred)
    diagnos = []
    for i in range(len(pred)):
        if pred[i][0][0] >= 0.05:
            #print(i, class_names[i], pred[i][0][0])
            diagnos.append(str(class_names[i]) + ' : ' + str(int(round(pred[i][0][0] * 100))) + '%')

    cam = gc.grad_cam(model, x_model_i, x_orig, predicted_class, "conv5_blk_scale", class_names)
    font = cv2.FONT_HERSHEY_SIMPLEX

    cv2.putText(cam, '  |  '.join(diagnos[:5]), (5, 20), font, fontScale=0.5,
                color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)

    cv2.putText(cam, '  |  '.join(diagnos[5:10]), (5, 50), font, fontScale=0.5,
                color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)

    return cam


UPLOAD_FOLDER = os.path.join('uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['image']
    f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)

    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            reversing = False
            
            if request.form.get('positive'):
                reversing = True
            
            if request.form.get('negative'):
                reversing = False
            
            print('Reversing:', reversing)
            dcm = dicom.read_file(file)
            arr = dcm.pixel_array
            h, w =arr.shape
            image = Image.fromarray(cv2.cvtColor(scipy.misc.imresize(arr, (h, w)), cv2.COLOR_GRAY2RGB))

            orig_im = prepare_image(image=image, target=(1024, 1024), preparation=False, reverse=reversing) / 255
            prep_img = prepare_image(image=image, target=(224, 224), preparation=True, reverse=reversing).reshape(1, 224, 224, 3)

            preds = model.predict(prep_img)
            results = grad_cam(model, class_names, orig_im, prep_img, preds)
            new_name = datetime.now().isoformat() + '_' + file.filename + '.png'
            f = os.path.join(app.config['UPLOAD_FOLDER'], new_name)
            cv2.imwrite(f, results)

    return render_template('index.html', user_image=new_name)

@app.route('/uploads/<filename>')
def uploads(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7008)
    #print('MODEL', model)
    #http_server = WSGIServer(('', 5000), app)
    #http_server.serve_forever()
