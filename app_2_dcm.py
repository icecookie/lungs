import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"
from werkzeug.contrib.fixers import ProxyFix
from flask import Flask, render_template, request, send_from_directory
import flask
from configparser import ConfigParser
from PIL import Image

import grad_cam as gc
import numpy as np
import cv2
import dicom
import scipy.misc
from datetime import datetime
from models.keras import ModelFactory


app = Flask(__name__, template_folder='templates')
app.wsgi_app = ProxyFix(app.wsgi_app)


global model
global class_names
config_file = "sample_config_2.ini"
cp = ConfigParser()
cp.read(config_file)
class_names = cp["DEFAULT"].get("class_names").split(",")
base_model_name = cp["DEFAULT"].get("base_model_name")
weights_path = 'brucechou1983_CheXNet_Keras_0.3.0_weights.h5'
model_factory = ModelFactory()
model = model_factory.get_model(
    class_names,
    model_name=base_model_name,
    use_base_weights=False,
    weights_path=weights_path)


def prepare_image(image, target, preparation=True, reverse=False):

    image = image.convert("RGB")

    image = image.resize(target)
    image = np.asarray(image)[:, :, ::-1]

    if reverse:
        image = cv2.bitwise_not(image)

    if preparation:

        image = image / 255
        mean = np.array([0.485, 0.456, 0.406])
        std = np.array([0.229, 0.224, 0.225])

        return (image - mean) / std #image to predict

    else:
        return image #original image


def grad_cam(model, class_names, x_orig, x_model, pred):

    x_orig = 255 * x_orig.squeeze()
    x_model_i = x_model
    predicted_class = np.argmax(pred)

    diagnos = []
    for i in range(len(pred)):
        if pred[i] >= 0.2:
            diagnos.append(str(class_names[i]) + ' : ' + str(int(round(pred[i] * 100))) + '%')

    if len(diagnos) == 0:
        diagnos.append('Normal')

    cam = gc.grad_cam(model, x_model_i, x_orig, predicted_class, "bn", class_names)
    font = cv2.FONT_HERSHEY_SIMPLEX

    cv2.putText(cam, '  |  '.join(diagnos[:5]), (5, 20), font, fontScale=0.5,
                color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)

    cv2.putText(cam, '  |  '.join(diagnos[5:10]), (5, 50), font, fontScale=0.5,
                color=(255, 255, 255), thickness=2, lineType=cv2.LINE_AA)

    return cam

def rebuild_prob(predicts):


    arr = [1.3268213269948912, 4.061172472440595, 1.1340548637759544, 0.7117812705842438, 2.8247002854727565,
          2.1494727008797504, 30.844666043918815, 3.042027916877259, 3.5559085952690155, 7.299318189487509,
          2.5415810216974295, 65.98246274394953, 4.906781114365756, 26.07994399651056]

    balancing = np.array(arr)
    predicts = predicts * balancing

    norm = np.linalg.norm(predicts)
    predicts = predicts / norm

    return predicts

UPLOAD_FOLDER = os.path.join('uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['image']
    f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)

    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            dcm = dicom.read_file(file)
            im_width = int(dcm.Rows)
            im_height = int(dcm.Columns)
            im_type = str(dcm.PhotometricInterpretation)

            center = int(dcm.WindowCenter)
            step = int(dcm.WindowWidth)

            left = center - step / 2
            right = center + step / 2

            
            if im_type == 'MONOCHROME1':
                reversing = True
            else:
                reversing = False 

            arr = dcm.pixel_array
            np.place(arr, arr <= left, left)
            np.place(arr, arr >= right, right)
            
            image = Image.fromarray(cv2.cvtColor(scipy.misc.imresize(arr, (im_height, im_width)), cv2.COLOR_GRAY2RGB))

            orig_im = prepare_image(image=image, target=(1024, 1024), preparation=False, reverse=reversing) / 255
            prep_img = prepare_image(image=image, target=(224, 224), preparation=True, reverse=reversing).reshape(1, 224, 224, 3)

            predicts = model.predict(prep_img)[0]

            results = grad_cam(model, class_names, orig_im, prep_img, rebuild_prob(predicts))
            new_name = datetime.now().isoformat() + '_' + file.filename + '.png'
            f = os.path.join(app.config['UPLOAD_FOLDER'], new_name)
            cv2.imwrite(f, results)

    return render_template('index.html', user_image=new_name)

@app.route('/uploads/<filename>')
def uploads(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)



if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7008)
